package MyTest;
import Task_2.Category1Impl;
import Task_2.Category2Impl;
import org.testng.Assert;
import org.testng.annotations.Test;
public class Task_2_Test {
    Category1Impl category1 = new Category1Impl();
    Category2Impl category2 = new Category2Impl();
    @Test
    public  void containsTestCategory1 (){
        Assert.assertTrue(category1.contains(),"True");
    }
    @Test
    public  void containsTestCategory2 (){
        Assert.assertTrue(category2.contains(),"False");
    }
}

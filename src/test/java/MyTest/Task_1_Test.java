package MyTest;
import Task_1.CatImpl;
import Task_1.DogImpl;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
public class Task_1_Test {
    CatImpl cat = new CatImpl();
    DogImpl dog = new DogImpl();
    @Test
    public void CatNotNullTest() {
        Assert.assertNotNull(cat);
    }
    @Test
    public void DogNotNullTest() {
        Assert.assertNotNull(dog);
    }
    @BeforeTest
    public void beforeTest() throws Exception {
        System.out.println("@BeforeTest");
    }
    @AfterTest
    public void afterTest() throws Exception {
        System.out.println("@AfterTest");
    }
    @Test
    public  void testPositiveCatGetSound(){
        Assert.assertEquals("Cats Meow",cat.getSound());
    }
    @Test
    public  void testNegativeCatGetSound(){
        Assert.assertEquals("Cats Gav",cat.getSound());
    }
    @Test
    public  void testPositiveDogGetSound(){
        Assert.assertEquals("Dogs Gav",dog.getSound());
    }
    @Test
    public  void testNegativeDogGetSound(){
        Assert.assertEquals("Dogs Meow",dog.getSound());
    }
}

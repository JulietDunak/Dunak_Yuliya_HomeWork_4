package MyTest;
import Task_3.Circle;
import Task_3.Square;
import org.testng.Assert;
import org.testng.annotations.Test;
public class Task_3_Test {
   Square square = new Square(2.5);
   Circle circle = new Circle(2.14);
 @Test
 public void testNegativeSquareGetArea() throws Exception {
     Assert.assertEquals(5.00, square.getArea());
   }
    @Test
    public void testPositiveSquareGetArea() throws Exception {
        Assert.assertEquals(6.25, square.getArea());
    }
    @Test
    public void testNegativeSquareGetPerimeter() throws Exception {
        Assert.assertEquals(10.50, square.getPerimeter());
    }
    @Test
    public void testPositiveSquareGetPerimeter() throws Exception {
        Assert.assertEquals(10.00, square.getPerimeter());
    }
    @Test
    public void testNegativeCircleGetArea() throws Exception {
        Assert.assertEquals(5.00, circle.getArea());
    }
    @Test
    public void testPositiveCircleGetArea() throws Exception {
        Assert.assertEquals(14.387237716379818, circle.getArea());
    }
    @Test
    public void testNegativeCircleGetPerimeter() throws Exception {
        Assert.assertEquals(10.50, circle.getPerimeter());
    }
    @Test
    public void testPositiveCircleGetPerimeter() throws Exception {
        Assert.assertEquals(13.446016557364315, circle.getPerimeter());
    }
}